# frozen_string_literal: true

require 'test_helper'

class LogTest < ActiveSupport::TestCase
  test 'log without user' do
    assert_raises(ActiveRecord::RecordInvalid) do
      assert_no_difference 'Log.count' do
        Log.warn msg: 'test'
      end
    end
  end

  test 'warn' do
    assert_difference 'Log.count' do
      Log.warn msg: 'test', user: users(:existed)
    end
  end
end
