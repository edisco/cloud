# frozen_string_literal: true

require 'test_helper'

class LanguageTest < ActiveSupport::TestCase
  test 'existed lang' do
    assert_not_nil Language['ru']
  end

  test 'unexisted lang' do
    assert_raises(Exception) do
      Language['tt']
    end
  end
end
