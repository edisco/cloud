# frozen_string_literal: true

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = users :existed
  end

  test 'delete' do
    assert_difference 'User.count', -1 do
      assert_difference 'UserWord.count', -@user.words.count do
        @user.destroy
      end
    end
  end
  test 'populate en ru' do
    wc = @user.words.length
    wt = @user.word_triples.length
    @user.populate_vocabulary native_lang: 'ru', learning_lang: 'en'
    assert @user.words.reload.length > wc
    assert @user.word_triples.reload.length > wt
  end

  test 'en ru voc tear' do
    assert_equal 0, Word.where(text: 'find').count
    @user.populate_vocabulary native_lang: 'ru', learning_lang: 'en'
    assert_equal 1, Word.where(text: 'find').count
    assert_equal 1, @user.words.joins(:word).where(words: { text: 'find' }).count
  end
end
