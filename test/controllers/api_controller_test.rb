# frozen_string_literal: true

require 'test_helper'

class ApiControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = users :existed
    ENV['GOOGLE_TOKEN'] = 'test'
  end

  test 'sing in with wrong params' do
    post api_sign_in_url
    assert_response :missing
    assert_equal '{"errors":[{"title":"wrong params"}]}', @response.body
  end

  test 'sing in new user' do
    assert_difference 'User.count' do
      post api_sign_in_url(email: 'new.user@gmail.com', google_token: 'test')
    end
    assert_response :success
    json = JSON.parse @response.body

    assert json['meta']['token'].present?
    assert json['meta']['new'].present? && json['meta']['new']
  end

  test 'sing in existed user' do
    assert_no_difference 'User.count' do
      post api_sign_in_url(email: @user.email, google_token: @user.google_token)
    end
    assert_response :success
    json = JSON.parse @response.body

    assert json['meta']['token'].present?
    assert !json['meta']['new']
  end

  test 'sigin with same token' do
    post api_sign_in_url(email: @user.email, google_token: @user.google_token)
    token = JSON.parse(@response.body)['meta']['token']
    assert !token.blank?
    post api_sign_in_url(email: @user.email, google_token: @user.google_token)
    assert_equal token, JSON.parse(@response.body)['meta']['token']
  end

  test 'intro wizard log unable to find lang' do
    assert_difference 'Log.count' do
      post api_wizard_setup_url(
        email: @user.email, token: @user.token,
        data: {
          native_lang: 'uk2',
          learning_lang: 'de'
        }
      )
    end
    assert_response :success
  end

  test 'intro wizard  uuid' do
    count = @user.word_triples.count
    post api_wizard_setup_url(
      email: @user.email, token: @user.token,
      data: {
        native_lang: 'ru',
        learning_lang: 'en'
      }
    )
    assert_response :success

    assert_not_nil @user.word_triples.last.uuid
    assert_not_nil @user.word_triples.last.front1
    assert count < @user.word_triples.count
  end

  test 'de ru wizard voc' do
    count = @user.word_triples.count
    post api_wizard_setup_url(
      email: @user.email, token: @user.token,
      data: {
        native_lang: 'ru',
        learning_lang: 'de'
      }
    )
    assert_response :success
    assert_equal 1, @user.words.joins(:word).where(words: { text: 'ging' }).count

    assert count < @user.word_triples.count
  end

  test 'de uk wizard voc' do
    count = @user.word_triples.count
    post api_wizard_setup_url(
      email: @user.email, token: @user.token,
      data: {
        native_lang: 'uk',
        learning_lang: 'de'
      }
    )
    assert_response :success
    assert_equal 1, @user.words.joins(:word).where(words: { text: 'йти' }).count

    assert count < @user.word_triples.count
  end

  test 'en ru wizard with overriding' do
    skip
    wt = WordTriple.find_or_create_by!(
      front1: Word.find_or_create_by(text: 'word1', language: Language['en']),
      front2: Word.find_or_create_by(text: 'word2', language: Language['en']),
      front3: Word.find_or_create_by(text: 'word3', language: Language['en']),
      back: Word.create!(text: 'быть', lang: 'en')
    )
    assert_equal 'en', wt.back.language.lang
    Word.create! text: 'быть', lang: 'ru'
    Word.create! text: 'писать', lang: 'en'
    post api_wizard_setup_url(
      email: @user.email, token: @user.token,
      data: {
        native_lang: 'ru',
        learning_lang: 'en'
      }
    )
    assert_response :success
    list = Word.where(text: 'писать')
    assert_equal 1, list.count
    assert_equal 'ru', list[0].language.lang
    wt.reload
    assert_equal 'быть', wt.back.text
    assert_equal 'ru', wt.back.language.lang
  end

  test 'get suggestions no word in voc' do
    stub_request(:get, /translation.googleapis.com/)
      .to_return(status: 200,
                 headers: { 'Content-Type': 'application/json; charset=UTF-8' },
                 body: '{
  "data": {
    "translations": [
      {
        "translatedText": "понуждать"
      }
    ]
  }
}')

    post api_translation_suggestions_url(
      email: @user.email, token: @user.token,
      data: {
        text: 'hustle',
        native_lang: 'ru',
        learning_lang: 'en'
      }
    )

    assert_response :success

    json =  JSON.parse(@response.body)

    attrs = json['data'].first['attributes']
    assert_equal 'hustle', attrs['front_text']
    assert_equal 'понуждать', attrs['back_text']
    assert_equal 'en', attrs['front_lang']
    assert_equal 'ru', attrs['back_lang']
  end

  test 'suggest existed' do
    UserWordPair.build @user, 'dog', 'собака2', 'en', 'ru'

    post api_translation_suggestions_url(
      email: @user.email, token: @user.token,
      data: {
        text: 'dog',
        native_lang: 'ru',
        learning_lang: 'en'
      }
    )
    assert_response :success

    json = JSON.parse(@response.body)
    assert_equal 2, json['data'].size
    assert_equal 'собака', json['data'].first['attributes']['back_text']
    assert_equal 'собака2', json['data'].last['attributes']['back_text']
  end
end
