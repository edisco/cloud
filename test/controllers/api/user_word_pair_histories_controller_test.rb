# frozen_string_literal: true

require 'test_helper'

class ApiUserWordPairHistoriesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = users :existed
    ENV['GOOGLE_TOKEN'] = 'test'
  end
  test 'index' do
    get api_user_word_pair_histories_url(email: @user.email, token: @user.token)
    assert_response :success
    json = JSON.parse @response.body
    assert_equal @user.word_pair_histories.where(trashed: false).size, json['data'].size
  end
  test 'index one' do
    uwph = user_word_pair_histories :one
    get api_user_word_pair_histories_url(email: @user.email, token: @user.token, uuid: uwph.uuid)
    assert_response :success
    json = JSON.parse @response.body
    assert_equal 1, json['data'].size
  end

  test 'create' do
    wp = user_word_pairs :one
    date = (DateTime.now - 10).to_formatted_s(:iso8601ms)
    params = {
      attributes: {
        correct: true,
        created_at: date,
        word_pair_uuid:  wp.uuid,
        version: 1

      }
    }

    assert_difference 'UserWordPairHistory.count' do
      post api_user_word_pair_histories_url(email: @user.email, token: @user.token, data: params)
      assert_response :success
    end

    json = JSON.parse @response.body
    assert_not_nil json['data'].first['uuid']
    assert_equal true, json['data'].first['attributes']['correct']
    assert_equal Time.parse(date).utc.to_s(:iso8601ms), json['data'].first['attributes']['created_at']
  end

  test 'create wrong uuid' do
    params = {
      attributes: {
        correct: true,
        word_pair_uuid:  'wp.uuid',
        version: 1

      }
    }
    assert_no_difference 'UserWordPairHistory.count' do
      post api_user_word_pair_histories_url(email: @user.email, token: @user.token, data: params)
      assert_response 404
    end
  end

  test 'update' do
    uwph = user_word_pair_histories :one
    uwph.trashed = true
    uwph.version += 1
    params = uwph.to_api
    patch api_user_word_pair_history_url(email: @user.email, token: @user.token, data: params, uuid: uwph.uuid)
    assert_response :success
    assert_equal true, uwph.reload.trashed
  end

  test 'delete' do
    uwph = user_word_pair_histories :one
    assert_no_difference 'Word.count' do
      assert_no_difference 'UserWord.count' do
        assert_no_difference 'WordPair.count' do
          assert_difference 'UserWordPairHistory.count', -1 do
            delete api_user_word_pair_history_url(email: @user.email, token: @user.token, uuid: uwph.uuid)
          end
        end
      end
    end
    assert_response :success
  end
end
