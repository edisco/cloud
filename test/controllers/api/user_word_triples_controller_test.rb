# frozen_string_literal: true

require 'test_helper'

class ApiUserWordTriplesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = users :existed
    ENV['GOOGLE_TOKEN'] = 'test'
  end

  test 'get' do
    get api_user_word_triples_url(email: @user.email, token: @user.token)
    assert_response :success
    json = JSON.parse @response.body
    assert_equal @user.word_triples.where(trashed: false).count, json['data'].size
  end

  test 'get one' do
    uwt = user_word_triples :fly

    get api_user_word_triples_url(email: @user.email, token: @user.token, uuid: uwt.uuid)
    assert_response :success
    json = JSON.parse @response.body
    assert_equal 1, json['data'].size
  end
  test 'get one pagination' do
    get api_user_word_triples_url(email: @user.email, token: @user.token, per: 1)
    json = JSON.parse @response.body
    size = json['data'].size
    while json['meta'] && json['meta']['pagination'] && json['meta']['pagination']['page'].present?
      get api_user_word_triples_url(email: @user.email, token: @user.token, per: 1, page: json['meta']['pagination']['page'].to_i + 1)
      json = JSON.parse @response.body
      size += json['data'].size
    end
    assert_equal @user.word_triples.where(trashed: false).size, size
  end

  test 'create' do
    f1 = user_words :one
    f2 = user_words :two
    f3 = user_words :user_hamster_en
    b = user_words :user_hamster_ru
    date = DateTime.now - 23
    params = {
      type: 'UserWordTriple',
      attributes: {
        front1_uuid: f1.uuid,
        front2_uuid: f2.uuid,
        front3_uuid: f3.uuid,
        back_uuid: b.uuid,
        stage: 1,
        version: 1,
        trashed: false,
        created_at: date.to_s(:iso8601ms)
      }
    }
    assert_no_difference 'Word.count' do
      assert_no_difference 'UserWord.count' do
        assert_difference 'WordTriple.count' do
          assert_difference 'UserWordTriple.count' do
            post api_user_word_triples_url(email: @user.email, token: @user.token, data: params)
          end
        end
      end
    end
    assert_response :success
    json = JSON.parse @response.body
    assert_not_nil json['data'].first['uuid']
    assert_equal date.utc.to_s(:iso8601ms), json['data'].first['attributes']['created_at']
  end

  test 'create wrong uuid' do
    f1 = user_words :one
    f2 = user_words :two
    f3 = user_words :user_hamster_en

    params = {
      type: 'UserWordTriple',
      attributes: {
        front1_uuid: f1.uuid,
        front2_uuid: f2.uuid,
        front3_uuid: f3.uuid,
        back_uuid: 'b.uuid',
        stage: 1,
        version: 1,
        trashed: false
      }
    }
    assert_no_difference 'Word.count' do
      assert_no_difference 'UserWord.count' do
        assert_no_difference 'WordTriple.count' do
          assert_no_difference 'UserWordTriple.count' do
            post api_user_word_triples_url(email: @user.email, token: @user.token, data: params)
          end
        end
      end
    end
    assert_response 404
  end

  test 'update' do
    uwt = user_word_triples :fly
    uw = user_words(:user_hamster_ru)
    date = DateTime.now - 2
    params = uwt.to_api
    params[:attributes][:back_uuid] = uw.uuid
    params[:attributes][:version] = uwt.version + 1
    params[:attributes][:created_at] = date.to_s(:iso8601ms)
    assert_no_difference 'Word.count' do
      assert_no_difference 'UserWord.count' do
        assert_difference 'WordTriple.count' do
          assert_no_difference 'UserWordTriple.count' do
            patch api_user_word_triple_url(email: @user.email, token: @user.token, data: params, uuid: uwt.uuid)
          end
        end
      end
    end
    assert_response :success
    # json = JSON.parse @response.body
    assert_equal uw.uuid, uwt.reload.back.uuid
    assert_equal uwt.created_at.utc.to_s(:iso8601ms), date.utc.to_s(:iso8601ms)
  end
  test 'destroy' do
    uwt = user_word_triples :fly
    assert_no_difference 'Word.count' do
      assert_no_difference 'UserWord.count' do
        assert_no_difference 'WordTriple.count' do
          assert_difference 'UserWordTriple.count', -1 do
            delete api_user_word_triple_url(email: @user.email, token: @user.token, uuid: uwt.uuid)
          end
        end
      end
    end
    assert_response :success
  end
end
