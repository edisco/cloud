# frozen_string_literal: true

require 'test_helper'

class ApiUserWordTripleHistoriesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = users :existed
    ENV['GOOGLE_TOKEN'] = 'test'
  end

  test 'index' do
    get api_user_word_triple_histories_url(email: @user.email, token: @user.token)
    assert_response :success
    json = JSON.parse @response.body
    assert_equal @user.word_triple_histories.where(trashed: false).size, json['data'].size
  end

  test 'index one' do
    uwth = user_word_triple_histories :one
    get api_user_word_triple_histories_url(email: @user.email, token: @user.token, uuid: uwth.uuid)
    assert_response :success
    json = JSON.parse @response.body
    assert_equal 1, json['data'].size
  end

  test 'create' do
    wp = user_word_triples :fly
    date = (DateTime.now - 10).to_formatted_s(:iso8601ms)
    params = {
      attributes: {
        correct: true,
        created_at: date,
        word_triple_uuid:  wp.uuid,
        version: 1
      }
    }

    assert_difference 'UserWordTripleHistory.count' do
      post api_user_word_triple_histories_url(email: @user.email, token: @user.token, data: params)
      assert_response :success
    end

    json = JSON.parse @response.body
    assert_not_nil json['data'].first['uuid']
    assert_equal true, json['data'].first['attributes']['correct']
    assert_equal Time.parse(date).utc.to_s(:iso8601ms), json['data'].first['attributes']['created_at']
  end

  test 'create wrong uuid' do
    params = {
      attributes: {
        correct: true,
        word_triple_uuid:  'wp.uuid',
        version: 1

      }
    }
    assert_no_difference 'UserWordTripleHistory.count' do
      post api_user_word_triple_histories_url(email: @user.email, token: @user.token, data: params)
      assert_response 404
    end
  end

  test 'update' do
    uwth = user_word_triple_histories :two
    uwth.trashed = true
    uwth.version += 1
    params = uwth.to_api

    patch api_user_word_triple_history_url(email: @user.email, token: @user.token, data: params, uuid: uwth.uuid)
    assert_response :success
    assert_equal true, uwth.reload.trashed
  end

  test 'delete' do
    uwth = user_word_triple_histories :one
    assert_no_difference 'Word.count' do
      assert_no_difference 'UserWord.count' do
        assert_no_difference 'WordTriple.count' do
          assert_difference 'UserWordTripleHistory.count', -1 do
            delete api_user_word_triple_history_url(email: @user.email, token: @user.token, uuid: uwth.uuid)
          end
        end
      end
    end
    assert_response :success
  end
end
