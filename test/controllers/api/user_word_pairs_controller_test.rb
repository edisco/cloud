# frozen_string_literal: true

require 'test_helper'

class ApiUserWordPairsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = users :existed
    ENV['GOOGLE_TOKEN'] = 'test'
  end

  test 'get user word pairs' do
    get api_user_word_pairs_url(email: @user.email, token: @user.token)
    assert_response :success
    json = JSON.parse @response.body
    assert_equal @user.word_pairs.where(trashed: false).count, json['data'].size
    uwp = @user.word_pairs.find_by uuid: json['data'].first['uuid']
    get api_user_words_url(email: @user.email, token: @user.token, uuid: json['data'].first['attributes']['front_uuid'])
    assert_response :success
    json = JSON.parse @response.body
    assert_equal 1, json['data'].size
    assert_equal uwp.front.word.text, json['data'].first['attributes']['text']
  end

  test 'create word pair' do
    front = user_words :user_hamster_en
    back = user_words :user_hamster_ru
    date = DateTime.now - 2
    params = {
      type: 'UserWordPair',
      attributes: {
        front_uuid: front.uuid,
        back_uuid: back.uuid,
        order: 1,
        stage: 1,
        version: 1,
        trashed: false,
        created_at: date.to_s(:iso8601ms)
      }
    }

    assert_difference 'WordPair.count' do
      assert_difference 'UserWordPair.count' do
        assert_no_difference 'UserWord.count' do
          post api_user_word_pairs_url(email: @user.email, token: @user.token, data: params)
        end
      end
    end
    assert_response :success
    json = JSON.parse @response.body
    record = json['data'].first
    assert record['uuid'].present?
    assert_equal 1, record['attributes']['version'].to_i
    assert_equal 1, record['attributes']['order'].to_i
    assert_equal 1, record['attributes']['stage'].to_i
    assert_equal date.utc.to_s(:iso8601ms), record['attributes']['created_at']

    uuid = record['uuid']

    assert @user.word_pairs.find_by(uuid: uuid).present?
  end
  test 'create wrong user word pair' do
    params = {
      type: 'UserWordPair',
      attributes: {
        front_uuid: 123,
        back_uuid: 231,
        order: 2,
        stage: 4,
        version: 11,
        trashed: false
      }
    }

    assert_no_difference 'WordPair.count' do
      assert_no_difference 'UserWordPair.count' do
        assert_no_difference 'UserWord.count' do
          post api_user_word_pairs_url(email: @user.email, token: @user.token, data: params)
        end
      end
    end
    assert_response 404
  end

  test 'update' do
    uwp = user_word_pairs :one

    params = uwp.to_api
    date = DateTime.now - 3
    params[:attributes][:back_uuid] = user_words(:user_hamster_ru).uuid
    params[:attributes][:version] = 717
    params[:attributes][:trashed] = true
    params[:attributes][:order] = 800
    params[:attributes][:stage] = 3
    params[:attributes][:created_at] = date.to_s(:iso8601ms)

    assert_difference 'WordPair.count' do
      assert_no_difference 'UserWordPair.count' do
        assert_no_difference 'UserWord.count' do
          patch api_user_word_pair_url(email: @user.email, token: @user.token, data: params, uuid: uwp.uuid)
        end
      end
    end
    assert_response :success
    json = JSON.parse @response.body
    record = json['data'].first
    uwp.reload
    assert_equal uwp.uuid, record['uuid']
    assert_equal uwp.version, record['attributes']['version']
    assert_equal uwp.stage, record['attributes']['stage']
    assert_equal uwp.order, record['attributes']['order']
    assert_equal uwp.trashed, record['attributes']['trashed']
    assert_equal uwp.created_at.utc.to_s(:iso8601ms), date.utc.to_s(:iso8601ms)
  end
  test 'wrong update' do
    patch api_user_word_pair_url(email: @user.email, token: @user.token, data: {}, uuid: '1')
    assert_response 404
  end

  test 'delete' do
    uwp = user_word_pairs :one
    assert_difference 'UserWordPair.count', -1 do
      assert_no_difference 'UserWord.count' do
        assert_no_difference 'WordPair.count' do
          delete api_user_word_pair_url email: @user.email, token: @user.token, uuid: uwp.uuid
        end
      end
    end
  end
  test 'update back word' do
    uwp = user_word_pairs :one
    uw = user_words :one
    params = uw.to_api
    params[:attributes][:text] = 'кот2'
    params[:attributes][:version] = 2
    patch api_user_word_url(email: @user.email, token: @user.token, data: params, uuid: uw.uuid)
    assert_response :success

    get api_user_word_pairs_url(email: @user.email, token: @user.token, uuid: uwp.uuid)
    assert_response :success
    json = JSON.parse @response.body
    assert_equal 1, json['data'].size
    assert_not_equal nil, json['data'].first['attributes']['back_uuid']
    uwp.reload
    assert_equal 'cat', uwp.word_pair.front.text
    assert_equal 'кот2', uwp.word_pair.back.text
  end

  test 'update back uuid' do
    uwp = user_word_pairs :one
    uw = user_words :user_hamster_en
    params = uwp.to_api
    params[:attributes][:back_uuid] = uw.uuid
    params[:attributes][:version] = uw.version + 1
    patch api_user_word_pair_url(email: @user.email, token: @user.token, data: params, uuid: uwp.uuid)
    assert_equal uw.uuid, uwp.reload.back.uuid
  end
end
