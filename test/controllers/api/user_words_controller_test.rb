# frozen_string_literal: true

require 'test_helper'

class ApiUserWordsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = users :existed
    ENV['GOOGLE_TOKEN'] = 'test'
  end

  test 'get words' do
    get api_user_words_url(email: @user.email, token: @user.token)
    assert_response :success
    json = JSON.parse @response.body
    assert_equal @user.words.where(trashed: false).count, json['data'].size
  end

  test 'word pagination' do
    wb = WordBuilder.new @user.id
    word_count = @user.words.where(trashed: false).count
    amount = 111
    assert_difference 'Word.count', amount do
      assert_difference 'UserWord.count', amount do
        amount.times.each do |id|
          wb.add_word text: "word_#{id}", language: Language.eng
        end
      end
    end

    get api_user_words_url(email: @user.email, token: @user.token)
    assert_response :success
    # puts @response.body
    json = JSON.parse @response.body
    total_size = json['data'].size
    assert_equal 50, total_size
    page = json['meta']['pagination']['page']
    total_count = json['meta']['pagination']['total_count']
    assert_equal 1, page
    prev_word = json['data'].first['text']

    while json['meta'] && json['meta']['pagination']
      page += 1
      get api_user_words_url(email: @user.email, token: @user.token, page: page)
      assert_response :success
      json = JSON.parse @response.body
      assert_not_equal prev_word, json['data'].first['attributes']['text']
      prev_word = json['data'].first['text']
      total_size += json['data'].size
    end
    assert_equal amount + word_count, total_size
    assert_equal amount + word_count, total_count
  end

  test 'update user word' do
    uw = user_words :one
    date = DateTime.now - 12
    params = uw.to_api
    version = params[:attributes][:version]
    params[:attributes][:text] = 'котенок'
    params[:attributes][:created_at] = date.to_s(:iso8601ms)
    params[:attributes][:version] += 1

    assert_difference 'Word.count' do
      assert_no_difference 'UserWord.count' do
        assert_no_difference 'Language.count' do
          patch api_user_word_url(email: @user.email, token: @user.token, data: params, uuid: uw.uuid)
        end
      end
    end
    json = JSON.parse @response.body

    assert_equal 'котенок', json['data'].first['attributes']['text']

    uw.reload
    assert_equal 'котенок', uw.word.text
    assert_equal version + 1, uw.version
    assert_equal date.utc.to_s(:iso8601ms), uw.created_at.to_s(:iso8601ms)
  end

  test 'update word with new lang' do
    uw = user_words :user_hamster_en
    params = uw.to_api
    params[:attributes][:lang] = 'uk'
    params[:attributes][:version] = 2
    assert_difference 'Word.count' do
      assert_no_difference 'UserWord.count' do
        assert_difference 'Language.count' do
          patch api_user_word_url(email: @user.email, token: @user.token, data: params, uuid: uw.uuid)
        end
      end
    end
    assert_response :success
    json = JSON.parse response.body
    assert_equal 'uk', json['data'].first['attributes']['lang']
  end

  test 'update word with new lang and new text' do
    uw = user_words :user_hamster_en
    params = uw.to_api
    params[:attributes][:text] = 'жопа'
    params[:attributes][:lang] = 'uk'
    params[:attributes][:version] = 2
    assert_difference 'Word.count' do
      assert_no_difference 'UserWord.count' do
        assert_difference 'Language.count' do
          patch api_user_word_url(email: @user.email, token: @user.token, data: params, uuid: uw.uuid)
        end
      end
    end
    assert_response :success
    json = JSON.parse response.body
    assert_equal 'uk', json['data'].first['attributes']['lang']
  end
  test 'insert new user word' do
    params = {
      type: 'UserWord',
      uuid: nil,
      attributes: {
        text: 'жопа',
        lang: 'ru',
        version: 0
      }
    }
    assert_difference 'Word.count' do
      assert_difference 'UserWord.count' do
        assert_no_difference 'Language.count' do
          post api_user_words_url(email: @user.email, token: @user.token, data: params)
        end
      end
    end
    assert_response :success
    json = JSON.parse @response.body
    assert json['data'].first['uuid'].present?
  end

  test 'insert new user word2' do
    date = DateTime.now - 10
    params = {
      type: 'UserWord',
      uuid: nil,
      attributes: {
        text: 'жопа',
        lang: 'ru',
        version: 10,
        trashed: true,
        created_at: date.to_s(:iso8601ms)
      }
    }

    post api_user_words_url(email: @user.email, token: @user.token, data: params)

    assert_response :success
    json = JSON.parse @response.body
    assert json['data'].first['uuid'].present?
    assert_equal true, json['data'].first['attributes']['trashed']
    assert_equal 10, json['data'].first['attributes']['version']
    assert_equal date.utc.to_s(:iso8601ms), json['data'].first['attributes']['created_at']
  end

  test 'insert new word that exists for another user' do
    u = users :user2
    u.add_word text: 'new word', language: Language.eng

    params = {
      type: 'UserWord',
      uuid: nil,
      attributes: {
        text: 'new word',
        lang: 'en',
        version: 0,
        trashed: false

      }
    }
    assert_no_difference 'Word.count' do
      assert_difference 'UserWord.count' do
        post api_user_words_url(email: @user.email, token: @user.token, data: params)
      end
    end
    assert_response :success
    json = JSON.parse @response.body
    assert_not_nil json['data'].first['attributes']['created_at']
  end

  test 'insert word without lang' do
    params = {
      type: 'UserWord',
      attributes: {
        text: 'жопа',
        version: 0
      }
    }

    assert_difference 'Word.count' do
      assert_difference 'UserWord.count' do
        assert_no_difference 'Language.count' do
          post api_user_words_url(email: @user.email, token: @user.token, data: params)
        end
      end
    end

    assert_response :success
  end

  test 'insert existed word' do
    uw = user_words :one
    params = uw.to_api
    params.delete :uuid
    assert_no_difference 'Word.count' do
      assert_no_difference 'UserWord.count' do
        assert_no_difference 'Language.count' do
          post api_user_words_url(email: @user.email, token: @user.token, data: params)
        end
      end
    end
    assert_response 403
  end

  test 'delete word' do
    uw = @user.words.first
    assert_no_difference 'Word.count' do
      assert_difference 'UserWord.count', -1 do
        delete api_user_word_url email: @user.email, token: @user.token, uuid: uw.uuid
      end
    end
  end

  test 'get one word' do
    uw = @user.words.first
    get api_user_words_url(email: @user.email, token: @user.token, uuid: uw.uuid)
    assert_response :success
    json = JSON.parse @response.body
    assert_equal 1, json['data'].size
  end

  test 'get wrong word' do
    get api_user_words_url(email: @user.email, token: @user.token, uuid: 'uw.uuid')
    assert_response :success
    json = JSON.parse @response.body
    assert_equal 0, json['data'].size
  end

  test 'get word in empty db' do
    UserWordPair.destroy_all
    UserWord.destroy_all
    get api_user_words_url(email: @user.email, token: @user.token)
    assert_response :success
    json = JSON.parse @response.body
    assert_equal 0, json['data'].size
    assert_equal({ 'timestamp' => 0.0 }, json['meta'])
  end

  test 'create word with same text' do
    uw = user_words :der_see
    params = uw.to_api
    params[:attributes][:gender] = 3
    post api_user_words_url(email: @user.email, token: @user.token, data: params)
    assert_response :success
  end

  test 'update word with same text' do
    uw = user_words :der_see
    params = uw.to_api
    params[:attributes][:gender] = 3
    params[:attributes][:version] = 3
    assert_difference 'Word.count' do
      assert_no_difference 'UserWord.count' do
        patch api_user_word_url(email: @user.email, token: @user.token, data: params, uuid: uw.uuid)
      end
    end
    assert_response :success
  end

  test 'get words with timestamp' do
    @user.words.last.update updated_at: DateTime.now
    get api_user_words_url(email: @user.email, token: @user.token, timestamp: (DateTime.now + 0.5).to_msec)
    assert_response :success
    json = JSON.parse @response.body

    assert_equal @user.words.where('updated_at >= :date', date: DateTime.now + 0.5).count, json['data'].size
  end

  test 'get new word after insert with timestamp' do
    timestamp = @user.words.maximum(:updated_at).to_msec + 1

    params = {
      type: 'UserWord',
      uuid: nil,
      attributes: {
        text: 'жопа',
        lang: 'ru',
        version: 0
      }
    }
    sleep 1
    post api_user_words_url(email: @user.email, token: @user.token, data: params)
    assert_response :success

    get api_user_words_url(email: @user.email, token: @user.token, timestamp: timestamp)
    assert_response :success

    json = JSON.parse @response.body
    assert_equal 1, json['data'].size
  end

  test 'get new word after update with timestamp' do
    timestamp = @user.words.maximum(:updated_at).to_msec + 1
    get api_user_words_url(email: @user.email, token: @user.token, timestamp: timestamp)
    assert_response :success

    json = JSON.parse @response.body
    assert_equal 0, json['data'].size

    uword = @user.words.last
    params = uword.to_api
    params[:attributes][:gender] = 3
    params[:attributes][:version] = 3
    sleep 0.1
    patch api_user_word_url(email: @user.email, token: @user.token, data: params, uuid: uword.uuid)
    assert_response :success

    get api_user_words_url(email: @user.email, token: @user.token, timestamp: timestamp)
    assert_response :success

    json = JSON.parse @response.body
    assert_equal 1, json['data'].size
  end
  test 'check if timestamp exists in meta response' do
    get api_user_words_url(email: @user.email, token: @user.token)
    assert_response :success
    json = JSON.parse @response.body

    assert json['meta']['timestamp'].present?
  end
end
