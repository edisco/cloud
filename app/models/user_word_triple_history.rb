# frozen_string_literal: true

class UserWordTripleHistory < ApplicationRecord
  PERMIT_PARAMS = [
    :uuid,
    data: [
      :type,
      :uuid,
      attributes: %i[
        correct
        word_triple_uuid
        created_at
        trashed
        version
      ]
    ]
  ].freeze
  belongs_to :user_word_triple
  belongs_to :user
  def to_api
    {
      type: self.class.name,
      uuid: uuid,
      attributes: {
        word_triple_uuid: user_word_triple.uuid,
        correct: correct,
        created_at: created_at.to_s(:iso8601ms),
        updated_at: updated_at.to_s(:iso8601ms),
        trashed: trashed,
        version: version
      }
    }
  end
end
