# frozen_string_literal: true

class User < ApplicationRecord
  include VocabularyBuilder
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, #:registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :word_triple_histories, class_name: 'UserWordTripleHistory', dependent: :destroy
  has_many :word_pair_histories, class_name: 'UserWordPairHistory', dependent: :destroy
  has_many :word_pairs, class_name: 'UserWordPair', dependent: :destroy
  has_many :word_triples, class_name: 'UserWordTriple', dependent: :destroy
  has_many :words, class_name: 'UserWord', dependent: :destroy
  has_many :logs, dependent: :destroy

  def self.create_test_user!
    raise 'EDISCO_TEST_TOKEN is nil' if ENV['EDISCO_TEST_TOKEN'].nil?
    u = User.find_by email: 'test@edisco.snakelab.cc'
    u.destroy if u.present?
    u = User.find_or_create_by(email: 'test@edisco.snakelab.cc') do |user|
      user.password = ENV['EDISCO_TEST_TOKEN']
    end
    u.password = ENV['EDISCO_TEST_TOKEN']
    u.token = ENV['EDISCO_TEST_TOKEN']

    u.save!
    u
  end

  def add_word(params)
    wb = WordBuilder.new self
    wb.add_word params
  end
end
