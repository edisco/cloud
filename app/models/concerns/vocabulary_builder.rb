# frozen_string_literal: true

module VocabularyBuilder
  def native_lang_fix(row, ll, nl)
    # fix forword that used to be with learning lang instead of native lang
    Word.joins(:language).where(text: row['back'], languages: { lang: ll }).each do |w|
      puts "Word #{w.inspect}"
      nw = Word.joins(:language).find_by text: row['back'], languages: { lang: nl }
      if nw.nil?
        w.lang = nl
        w.save
      else

        w.word_pairs.each do |wp|
          wp.back_id = nw.id if wp.back_id == w.id
          wp.save!
        end

        w.word_triples.each do |wt|
          puts "WordTriple #{wt.inspect}"
          next unless wt.back_id == w.id
          nwt = WordTriple.find_by front1: wt.front1, front2: wt.front2, front3: wt.front3, back: nw.id
          puts "NewWordTriple #{nwt.inspect}"
          if nwt.nil?
            wt.back_id = nw.id
            wt.save!
          else
            wt.update! back_id: nw.id
          end
        end

        w.user_words.each do |uw|
          uw.word_id = nw.id
          uw.save!
        end
        w.destroy
      end
    end
  end

  def populate_vocabulary(params)
    data = load_yaml params
    return if data.nil?
    nl = params[:native_lang]
    ll = params[:learning_lang]
    data.each do |row|
      front1, ufront1 = get_word row['front1'], ll
      front2, ufront2 = get_word row['front2'], ll
      front3, ufront3 = get_word row['front3'], ll
      back, uback = get_word row['back'], nl
      wt = WordTriple.find_or_create_by! front1: front1, front2: front2, front3: front3, back: back
      UserWordTriple.find_or_create_by! word_triple: wt, user: self, front1: ufront1, front2: ufront2, front3: ufront3, back: uback
    end
  end

  private

  def get_word(text, lang)
    w = Word.find_or_create_by text: text, language: Language[lang]
    uw = UserWord.find_or_create_by word: w, user: self
    [w, uw]
  end

  def load_yaml(params)
    fn = "#{params[:learning_lang]}_#{params[:native_lang]}.yml"
    fn = File.expand_path("voc/#{fn}", File.dirname(__FILE__))
    if File.exist? fn
      YAML.safe_load(File.read(fn))
    else
      Log.warn title: "Unable to find #{fn}", msg: params.to_s, user: self
      nil
    end
  end
end
