# frozen_string_literal: true

class Word < ApplicationRecord
  belongs_to :language
  has_many :user_words

  def lang=(value)
    self.language = Language.find_or_create_by(lang: value)
  end

  def word_pairs
    WordPair.where('front_id =:id or back_id = :id', id: id)
  end

  def word_triples
    WordTriple.where('front1_id =:id or front2_id =:id or front3_id =:id or back_id = :id', id: id)
  end
end
