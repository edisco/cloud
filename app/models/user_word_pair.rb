# frozen_string_literal: true

class UserWordPair < ApplicationRecord
  PERMIT_PARAMS = [
    :uuid,
    data: [
      :type,
      :uuid,
      attributes: %i[front_uuid back_uuid version trashed order stage created_at]
    ]
  ].freeze
  belongs_to :user
  belongs_to :word_pair
  belongs_to :front, class_name: 'UserWord'
  belongs_to :back, class_name: 'UserWord'
  has_many :user_word_pair_histories, dependent: :destroy

  def to_api
    {
      type: self.class.name,
      uuid: uuid,
      attributes: {
        front_uuid: front.uuid,
        back_uuid: back.uuid,
        trashed: trashed,
        version: version,
        order: order,
        stage: stage,
        created_at: created_at.to_s(:iso8601ms),
        updated_at: updated_at.to_s(:iso8601ms)

      }
    }
  end

  def self.build(user, front_text, back_text, front_lang, back_lang)
    wb = WordBuilder.new user
    front_w = wb.add_word text: front_text, language: Language[front_lang]
    back_w = wb.add_word text: back_text, language: Language[back_lang]
    wp = WordPair.new front_id: front_w.word_id, back_id: back_w.word_id
    wp.save!
    uwp = UserWordPair.new user: user, word_pair: wp, front: front_w, back: back_w
    uwp.save!
  end
end
