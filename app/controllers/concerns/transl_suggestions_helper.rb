# frozen_string_literal: true

require 'google/apis/translate_v2'

class WordPairTemplate
  def initialize(fromWordPair: nil, frontText: nil, frontLang: nil, backLang: nil, backText: nil)
    @front_text = frontText
    @back_text = backText
    @front_lang = frontLang
    @back_lang = backLang

    if fromWordPair.present? # rubocop:disable  Style/GuardClause
      @front_text = fromWordPair.front.text
      @back_text = fromWordPair.back.text
      @front_lang = fromWordPair.front.language.lang
      @back_lang = fromWordPair.back.language.lang
    end
  end

  def to_api
    {
      type: self.class.name,
      attributes: {
        front_text: @front_text,
        back_text: @back_text,
        front_lang: @front_lang,
        back_lang: @back_lang
      }
    }
  end
end

module TranslSuggestionsHelper
  def suggest(text, native_lang, learning_lang)
    return [] if text.nil? || native_lang.nil? || learning_lang.nil?
    # looking already existed
    front_word = Word.find_by text: text, language: Language[learning_lang]
    list = front_word.present? && WordPair.where(front_id: front_word.id).limit(5) || []
    unless list.empty?
      return list.map do |wp|
        WordPairTemplate.new fromWordPair: wp
      end

    end

    # if nothing has found. Make google translate help us
    translate = Google::Apis::TranslateV2::TranslateService.new
    translate.key = ENV['GOOGLE_TRANSLATE_API_KEY']

    result = translate.list_translations(text, native_lang, source: learning_lang)
    list = result.translations.to_a.take 5
    list.map do |trans|
      WordPairTemplate.new frontText: text, frontLang: learning_lang, backText: trans.translated_text, backLang: native_lang
    end
  end
end
