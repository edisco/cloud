# frozen_string_literal: true

require 'google/api_client/client_secrets'

module AuthHelpers
  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless User.where(token: token).first
    end
  end

  def test_token?(token)
    Rails.env.test? && ENV['GOOGLE_TOKEN'] == token || test_token_defined? && Devise.secure_compare(token, ENV['EDISCO_TEST_TOKEN'])
  end

  def test_token_defined?
    ENV['EDISCO_TEST_TOKEN'].present? && ENV['EDISCO_TEST_TOKEN'].length >= 8
  end

  def auth?(token, email)
    return true if test_token? token

    secrets = Google::APIClient::ClientSecrets.load

    validator = GoogleIDToken::Validator.new
    begin
      payload = validator.check(token, secrets.client_id, nil)
      email = payload['email']
    rescue => e
      Rails.logger.fatal "unable to validate google token: #{e}"
      return false
      # report "Cannot validate: #{e}"
    end
    email.present? && Devise.secure_compare(email.downcase, email)
  end
end
