# frozen_string_literal: true

class ApiController < ActionController::API
  include ApiHelpers
  include AuthHelpers
  extend AuthHelpers
  include TranslSuggestionsHelper

  before_action :check_token!, except: [:sign_in]
  around_action :wrap_in_transaction, except: [:sign_in], if: -> { !Rails.env.test? }
  around_action :error_handler, if: -> { !Rails.env.test? }

  def sign_in
    if get_params[:email].present?
      email = get_params[:email].downcase
      # check google token
      unless auth?(get_params[:google_token], email)
        render_error 'wrong token'
        return
      end

      user =
        User.find_by(email: email) ||
        User.create!(email: email, password: Devise.friendly_token.first(8))

      token = user.token
      token = generate_authentication_token if token.blank?

      user.update! google_token: get_params[:google_token], token: token

      render json: {
        meta: { token: token, 'new': user.new_user }
      }
    else
      render_error 'wrong params'
    end
  end

  def index
    page = get_params[:page] || 1
    per = get_params[:per] || 50
    uuid = get_params[:uuid]

    where = yield uuid
    # must be in mseconds since epoch in utc format
    timestamp = get_params[:timestamp]
    if timestamp.present?
      timestamp = Time.from_msec(timestamp).to_datetime
      # puts " server #{(timestamp).to_s(:iso8601ms)}"
      where = where.where('updated_at > :timestamp', timestamp: timestamp)
    end
    max_timestamp = where.maximum(:updated_at)
    # puts "updated_at max = #{max_timestamp.to_s(:iso8601ms)}}" if max_timestamp.present?
    max_timestamp = max_timestamp.to_msec
    # puts "max_timestamp = #{max_timestamp}"

    list = where.page(page).per(per)
    render_resp list, 200, max_timestamp
  end

  def wizard_setup
    @user.populate_vocabulary((params.permit(data: %i[native_lang learning_lang]))[:data]) # rubocop:disable Style/RedundantParentheses
    render_resp nil
  end

  def translation_suggestions
    p = get_params(data: %i[text native_lang learning_lang])
    data = p[:data]
    if data.nil?
      render_resp
    else
      list = suggest data[:text], data[:native_lang], data[:learning_lang]
      render_resp list
    end
  end

  protected

  def render_resp(list = nil, code = 200, max_timestamp = nil)
    resp = {}
    unless list.nil?
      list = [list] unless list.respond_to? :to_a
      resp = { data: list.to_a.map(&:to_api), meta: {} }

      if get_pagination(list).present?
        resp[:meta] = { pagination: get_pagination(list) }
      end
      resp[:meta][:timestamp] = max_timestamp if max_timestamp.present?
      resp.delete(:meta) if resp[:meta].empty?
    end
    render json: resp, status: code
  end

  def get_pagination(list)
    list.respond_to?(:last_page?) && !list.empty? && !list.last_page? && { page: list.current_page, total_count: list.total_count } || nil
  end

  def check_token!
    @user = User.find_by email: get_params[:email]
    valid = @user.present? && get_params[:token].present? && Devise.secure_compare(@user.token, get_params[:token])
    render_error 'wrong token' unless valid
  end

  def get_params(*permits)
    params.permit(%i[email google_token token page per uuid timestamp] + permits)
  end

  def wrap_in_transaction
    if test_token_defined? && Devise.secure_compare(get_params[:token], ENV['EDISCO_TEST_TOKEN'])
      ActiveRecord::Base.transaction do
        puts 'wrap_in_transaction1'
        yield
        puts 'wrap_in_transaction2'
        raise ActiveRecord::Rollback
      end
    else
      yield
    end
  end

  def error_handler
    yield
  rescue StandardError => e
    Log.safe_error user: @user, title: 'error_handler', msg: e.message, backtrace: e.backtrace
    render json: build_errors(['oops']), status: 500
  end
end
