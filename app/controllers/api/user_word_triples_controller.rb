# frozen_string_literal: true

class Api::UserWordTriplesController < ApiController
  before_action :permit_params!, except: [:index]
  before_action :check_front_and_back, only: %i[create update]

  def index
    super do |uuid|
      if uuid.present?
        @user.word_triples.where(uuid: uuid)
      else
        @user.word_triples.where(trashed: false)
      end
    end
  end

  def create
    wt = WordTriple.find_or_create_by front1_id: @front_uw1.word.id, front2_id: @front_uw2.word.id, front3_id: @front_uw3.word.id, back_id: @back_uw.word.id
    uwt = @user.word_triples.find_or_create_by word_triple_id: wt.id, front1_id: @front_uw1.id, front2_id: @front_uw2.id, front3_id: @front_uw3.id, back_id: @back_uw.id
    tweak_attributes @attributes
    uwt.update! @attributes
    render_resp uwt.reload, 201
  end

  def update
    uwt = @user.word_triples.find_by uuid: @params[:uuid]

    if uwt.nil?
      render_not_found
      return
    end
    tweak_attributes @attributes

    if @attributes[:version].to_i > uwt.version
      uwt.update! @attributes.merge(
        word_triple:
          WordTriple.find_or_create_by(front1_id: @front_uw1.word.id, front2_id: @front_uw2.word.id, front3_id: @front_uw3.word.id, back_id: @back_uw.word.id),
        front1: @front_uw1,
        front2: @front_uw2,
        front3: @front_uw3,
        back: @back_uw
      )

      render_resp uwt
    else
      render_ver_outdated
    end
  end

  def destroy
    uwt = @user.word_triples.find_by uuid: @params[:uuid]
    if uwt.nil?
      render_not_found
      return
    end
    uwt.destroy!
    render_resp
  end

  private

  def permit_params!
    @params = params.permit UserWordTriple::PERMIT_PARAMS
    @attributes = @params[:data] && @params[:data][:attributes] || {}
  end

  def tweak_attributes(attributes)
    attributes.delete(:front1_uuid) if attributes[:front1_uuid].present?
    attributes.delete(:front2_uuid) if attributes[:front2_uuid].present?
    attributes.delete(:front3_uuid) if attributes[:front3_uuid].present?
    attributes.delete(:back_uuid) if attributes[:back_uuid].present?
  end

  def check_front_and_back
    @front_uw1 = @user.words.find_by uuid: @attributes[:front1_uuid]
    if @front_uw1.nil?
      render_error "unable to find front user word 1 #{@attributes[:front1_uuid]}"
      return
    end

    @front_uw2 = @user.words.find_by uuid: @attributes[:front2_uuid]
    if @front_uw2.nil?
      render_error "unable to find front user word 2 #{@attributes[:front2_uuid]}"
      return
    end

    @front_uw3 = @user.words.find_by uuid: @attributes[:front3_uuid]
    if @front_uw1.nil?
      render_error "unable to find front user word 3 #{@attributes[:front3_uuid]}"
      return
    end

    @back_uw = @user.words.find_by uuid: @attributes[:back_uuid]

    return render_error "unable to find back user word #{@attributes[:back_uuid]}" if @back_uw.nil?
  end
end
