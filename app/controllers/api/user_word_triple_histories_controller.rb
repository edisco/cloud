# frozen_string_literal: true

class Api::UserWordTripleHistoriesController < ApiController
  before_action :permit_params!, except: [:index]

  def index
    super do |uuid|
      if uuid.present?
        @user.word_triple_histories.where(uuid: uuid)
      else
        @user.word_triple_histories.where(trashed: false)
      end
    end
  end

  def create
    uwt = @user.word_triples.find_by uuid: @attributes[:word_triple_uuid]

    if uwt.nil?
      render_error "Unable to find record #{@params[:uuid]}"
      return
    end
    tweak_attributes @attributes
    uwph = UserWordTripleHistory.create! @attributes.merge(user: @user, user_word_triple: uwt)
    render_resp uwph.reload, 201
  end

  def update
    uwth = @user.word_triple_histories.find_by uuid: @params[:uuid]
    if uwth.nil?
      render_not_found
      return
    end

    uwt = @user.word_triples.find_by uuid: @attributes[:word_triple_uuid]
    if uwt.nil?
      render_error "Unable to find record #{@params[:uuid]}"
      return
    end

    tweak_attributes @attributes
    if @attributes[:version].to_i > uwth.version
      uwth.update! @attributes.merge(user_word_triple: uwt)
      render_resp uwth
    else
      render_ver_outdated
    end
  end

  def destroy
    uwth = @user.word_triple_histories.find_by uuid: @params[:uuid]
    if uwth.nil?
      render_not_found
      return
    end
    uwth.destroy!
    render_resp
  end

  private

  def permit_params!
    @params = params.permit UserWordTripleHistory::PERMIT_PARAMS
    @attributes = @params[:data] && @params[:data][:attributes] || {}
  end

  def tweak_attributes(attributes)
    attributes.delete(:word_triple_uuid) if attributes[:word_triple_uuid].present?
    attributes[:created_at] = DateTime.strptime(attributes[:created_at], Time::DATE_FORMATS[:iso8601ms]) if attributes[:created_at].present?
  end
end
