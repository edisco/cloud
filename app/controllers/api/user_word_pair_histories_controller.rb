# frozen_string_literal: true

class Api::UserWordPairHistoriesController < ApiController
  before_action :permit_params!, except: [:index]

  def index
    super do |uuid|
      if uuid.present?
        @user.word_pair_histories.where(uuid: uuid)
      else
        @user.word_pair_histories.where(trashed: false)
      end
    end
  end

  def create
    uwp = @user.word_pairs.find_by uuid: @attributes[:word_pair_uuid]

    if uwp.nil?
      render_error "Unable to find record #{@params[:uuid]}"
      return
    end
    tweak_attributes @attributes
    uwph = UserWordPairHistory.create! @attributes.merge(user: @user, user_word_pair: uwp)
    render_resp uwph.reload, 201
  end

  def update
    uwph = @user.word_pair_histories.find_by uuid: @params[:uuid]

    if uwph.nil?
      render_not_found
      return
    end
    uwp = @user.word_pairs.find_by uuid: @attributes[:word_pair_uuid]
    if uwp.nil?
      render_error "Unable to find record #{@params[:uuid]}"
      return
    end
    tweak_attributes @attributes
    if @attributes[:version].to_i > uwph.version
      uwph.update! @attributes.merge(user_word_pair: uwp)
      render_resp uwph
    else
      render_ver_outdated
    end
  end

  def destroy
    uwph = @user.word_pair_histories.find_by uuid: @params[:uuid]
    if uwph.nil?
      render_not_found
      return
    end
    uwph.destroy!
    render_resp
  end

  private

  def permit_params!
    @params = params.permit UserWordPairHistory::PERMIT_PARAMS
    @attributes = @params[:data] && @params[:data][:attributes] || {}
  end

  def tweak_attributes(attributes)
    attributes.delete(:word_pair_uuid) if attributes[:word_pair_uuid].present?
    attributes[:created_at] = DateTime.strptime(attributes[:created_at], Time::DATE_FORMATS[:iso8601ms]) if attributes[:created_at].present?
  end
end
