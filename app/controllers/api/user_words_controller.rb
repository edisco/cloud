# frozen_string_literal: true

class Api::UserWordsController < ApiController
  def index
    super do |uuid|
      if uuid.present?
        @user.words.where(uuid: uuid)
      else
        @user.words.where(trashed: false)
      end
    end
  end

  def create
    para = params.permit(UserWord::PERMIT_PARAMS)
    attributes = para[:data][:attributes]

    user_word = @user.words.joins(:word).joins(:language).where(words: get_only_word_attributes(attributes).to_h, languages: { lang: attributes[:lang] })
    if user_word.present?
      render_resp nil, 403
      return
    end

    user_word = UserWord.new get_only_user_word_attributes(attributes).merge(user: @user)
    user_word.word = Word.find_or_create_by get_only_word_attributes(attributes)

    user_word.save!
    render_resp user_word.reload, 201
  end

  def update
    para = params.permit UserWord::PERMIT_PARAMS
    user_word = @user.words.find_by uuid: para[:data][:uuid]

    if user_word.nil?
      render_not_found
      return
    end
    attributes = para[:data][:attributes]
    tweak_lang_attr attributes

    if attributes[:version].to_i > user_word.version
      new_word = Word.find_or_create_by get_only_word_attributes(attributes)

      user_word.user_word_pairs_as_front.each do |uwp|
        uwp.word_pair.update! front: new_word
      end

      user_word.user_word_pairs_as_back.each do |uwp|
        uwp.word_pair.update! back: new_word
      end
      user_word.update! get_only_user_word_attributes(attributes).merge(word: new_word)

      render_resp user_word.reload
    else
      render_ver_outdated
    end
  end

  def destroy
    uuid = get_params[:uuid]

    uw = @user.words.find_by uuid: uuid
    if uw.present?
      uw.destroy!
      render_resp
    else
      render_not_found
    end
  end

  private

  def tweak_lang_attr(params)
    if params[:lang].present?
      params[:language] = Language.find_or_create_by(lang: params[:lang])
      params.delete :lang
    else
      params[:language] ||= Language.eng
    end
  end

  def get_only_word_attributes(params)
    par = params.reject { |k, _v| k.match(/^version$/i) }
    par = par.reject { |k, _v| k.match(/^trashed$/i) }
    par = par.reject { |k, _v| k.match(/^created_at$/i) }
    tweak_lang_attr par
    par
  end

  def get_only_user_word_attributes(params)
    par = params.select do |k, _v|
      k.match(/^version$/i) || k.match(/^trashed$/i) || k.match(/^created_at$/i)
    end
    par
  end
end
