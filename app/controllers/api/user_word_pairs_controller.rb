# frozen_string_literal: true

class Api::UserWordPairsController < ApiController
  before_action :permit_params!, except: [:index]
  before_action :check_front_and_back, only: %i[create update]

  def index
    super do |uuid|
      if uuid.present?
        @user.word_pairs.where(uuid: uuid)
      else
        @user.word_pairs.where(trashed: false)
      end
    end
  end

  def create
    wp = WordPair.find_or_create_by front_id: @front_uw.word_id, back_id: @back_uw.word_id
    uwp = @user.word_pairs.find_or_create_by word_pair_id: wp.id, front_id: @front_uw.id, back_id: @back_uw.id
    tweak_attributes @attributes
    uwp.update! @attributes

    render_resp uwp.reload, 201
  end

  def update
    uwp = @user.word_pairs.find_by uuid: @params[:uuid]

    if uwp.nil?
      render_not_found
      return
    end
    tweak_attributes @attributes
    if @attributes[:version].to_i > uwp.version
      uwp.update! @attributes.merge(word_pair: WordPair.find_or_create_by(front_id: @front_uw.word_id, back_id: @back_uw.word_id), front: @front_uw, back: @back_uw)
      render_resp uwp
    else
      render_ver_outdated
    end
  end

  def destroy
    uwp = @user.word_pairs.find_by uuid: @params[:uuid]
    if uwp.nil?
      render_not_found
      return
    end
    uwp.destroy!
    render_resp
  end

  private

  def permit_params!
    @params = params.permit UserWordPair::PERMIT_PARAMS
    @attributes = @params[:data] && @params[:data][:attributes] || {}
  end

  def check_front_and_back
    @front_uw = @user.words.find_by uuid: @attributes[:front_uuid]
    if @front_uw.nil?
      render_error "unable to find front user word #{@attributes[:front_uuid]}"
      return
    end

    @back_uw = @user.words.find_by uuid: @attributes[:back_uuid]
    return render_error "unable to find back user word #{@attributes[:back_uuid]}" if @back_uw.nil?
  end

  def find_word_pair(_attributes)
    @user.word_pairs.joins(:word_pairs).where(user_words: {})
  end

  def tweak_attributes(attributes)
    attributes.delete(:front_uuid) if attributes[:front_uuid].present?
    attributes.delete(:back_uuid) if attributes[:back_uuid].present?
  end
end
