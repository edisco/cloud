# frozen_string_literal: true

class MainController < ApplicationController
  def index; end

  def test; end

  def authenticate_user!
    true
  end
end
