# frozen_string_literal: true

class WordBuilder
  def initialize(user)
    @user = if user.is_a? User
              user
            else
              User.find user
            end
  end

  def add_word(params)
    w = Word.find_or_create_by(params.reject { |k, _v| k == :updated_at })
    UserWord.find_or_create_by! word_id: w.id, user_id: @user.id do |uw|
      uw.created_at = params[:created_at]
      uw.updated_at = params[:updated_at]
    end
  end
end
