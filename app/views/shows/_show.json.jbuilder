# frozen_string_literal: true

json.extract! show, :id, :tag, :created_at, :updated_at
json.url show_url(show, format: :json)
