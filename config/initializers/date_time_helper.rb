
Time.class_eval do
	def to_msec
		(self.to_f * 1000).round
	end
	def self.from_msec msec
		Time.at(msec.to_i / 1000.0)
	end
end
DateTime.class_eval do
	def to_msec
		(self.to_f * 1000).round
	end
	def self.from_msec msec
		Time.at(msec.to_i / 1000.0)
	end
end
NilClass.class_eval do
	def to_msec
		0
	end
end