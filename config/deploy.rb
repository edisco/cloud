# config valid only for current version of Capistrano
lock '3.8.2'

set :application, 'edisco'
set :repo_url, 'git@gitlab.com:edisco/cloud.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
set :branch, :release
# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '~/projects/edisco'

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
append :linked_files, '.env'

# Default value for linked_dirs is []
append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/puma', 'tmp/sockets', 'public/system', 'certs'

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

# set :puma_jungle_conf, '/etc/puma.conf'
# set :puma_run_path, '/usr/local/bin/run-puma'
set :puma_state, "#{shared_path}/tmp/puma/state"
set :puma_pid, "#{shared_path}/tmp/puma/pid"
set :puma_preload_app, true
set :rails_env, 'production'

set :puma_conf, "#{release_path}/config/puma.conf"
# Set the roles where the let's encrypt process should be started
# Be sure at least one server has primary: true
# default value: :web
set :lets_encrypt_roles, :web

# Optionally set the user to use when installing on the remote system
# default value: nil
set :lets_encrypt_user, 'snake'

# Set it to true to use let's encrypt staging servers
# default value: false
# set :lets_encrypt_test, true

# Set your let's encrypt account email (required)
# The account will be created if no private key match
# default value: nil
set :lets_encrypt_email, 'max.nedelchev@gmail.com'

# Set the path to your let's encrypt account private key
# default value: "#{fetch(:lets_encrypt_email)}.account_key.pem"
set :lets_encrypt_account_key, "~/#{fetch(:lets_encrypt_email)}.account_key.pem"

# Set the path from where you are serving your static assets
# default value: "#{release_path}/public"
set :lets_encrypt_challenge_public_path, "#{release_path}/public"

# Set the path where the new certs are going to be saved
# default value: "#{shared_path}/ssl/certs"
set :lets_encrypt_output_path, '/etc/nginx/ssl'

# Set the local path where the certs will be saved
# default value: "~/certs"
set :lets_encrypt_local_output_path, '~/certs'

# Set the minimum time that the cert should be valid
# default value: 30
set :lets_encrypt_days_valid, 90
