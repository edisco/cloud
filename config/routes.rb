Rails.application.routes.draw do
  namespace :api, format: :json do
    post :sign_in
    post :wizard_setup
    post :translation_suggestions

    resources :user_words, only: %i[index create update destroy], param: :uuid
    resources :user_word_pairs, only: %i[index create update destroy], param: :uuid
    resources :user_word_triples, only: %i[index create update destroy], param: :uuid
    resources :user_word_pair_histories, only: %i[index create update destroy], param: :uuid
    resources :user_word_triple_histories, only: %i[index create update destroy], param: :uuid
  end

  # ActiveAdmin.routes(self)
  # devise_for :user
  namespace :main do
    get :index
    get :test
  end
  root to: 'main#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
