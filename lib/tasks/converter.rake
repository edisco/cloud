# frozen_string_literal: true

require 'open-uri'

def reap_word(txt)
  txt.strip.split(%r{[, /]}ix).delete_if(&:blank?).first
end

namespace :converter do
  desc 'Convert from html to yaml'
  task get_en_ru: :environment do
    url = 'https://www.native-english.ru/grammar/irregular-verbs'
    puts "Downloading a page from #{url}"
    f = open url
    puts 'Parsing via Nokogiri'
    doc = Nokogiri::HTML(f)
    puts 'Done'
    table = []
    doc.css('div.article table.table tbody tr').each do |row|
      spliter = /,\W/i
      verb1 = row.at_xpath('td[1]').text.strip.split(spliter).first
      verb2 = row.at_xpath('td[2]').text.strip.split(spliter).first
      verb3 = row.at_xpath('td[3]').text.strip.split(spliter).first
      transl = row.at_xpath('td[4]').text.strip.split(spliter).first
      puts "#{verb1}, #{verb2}, #{verb3} - #{transl}"
      table << { 'front1' => verb1, 'front2' => verb2, 'front3' => verb3, 'back' => transl }
    end
    fn = "#{Rails.root}/app/models/concerns/voc/en_ru_auto.yml"

    open(fn, 'w') { |f2| f2.write table.to_yaml.to_s }
  end
  desc 'Convert from de ru html to yaml'
  task get_de_ru: :environment do
    url = 'https://audio-class.ru/deutsch/deutsche-verben.php'
    puts "Downloading a page from #{url}"
    f = open url
    puts 'Parsing via Nokogiri'
    doc = Nokogiri::HTML(f)
    puts 'Done'
    table = []
    doc.css('div.table6play table tbody tr').each do |row|
      verb1 = reap_word row.at_xpath('td[3]').text
      verb2 = reap_word row.at_xpath('td[5]').text
      verb3 = reap_word row.at_xpath('td[6]').text
      transl = reap_word row.at_xpath('td[2]').text
      puts "#{verb1}, #{verb2}, #{verb3} - #{transl}"
      table << { 'front1' => verb1, 'front2' => verb2, 'front3' => verb3, 'back' => transl }
    end
    fn = "#{Rails.root}/app/models/concerns/voc/de_ru_auto.yml"

    open(fn, 'w') { |f2| f2.write table.to_yaml.to_s }
  end
end
