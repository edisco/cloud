# frozen_string_literal: true

namespace :user do
  desc 'TODO'
  task clear_voc: :environment do
    u = User.find_by(email: 'snake@area69-mtb.org')
    # u.words.destroy_all
    # u.word_pairs.destroy_all
    u.destroy
  end
end
