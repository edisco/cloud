# frozen_string_literal: true

require 'google/apis/translate_v2'
namespace :translate do
  desc 'TODO'
  task test: :environment do
    translate = Google::Apis::TranslateV2::TranslateService.new
    translate.key = ENV['GOOGLE_TRANSLATE_API_KEY']
    result = translate.list_translations('Hello world!', 'es', source: 'en')
    puts result.translations.first.translated_text
    result = translate.list_translations('prone to', 'ru', source: 'en')
    puts result.translations.first.translated_text
  end
end
