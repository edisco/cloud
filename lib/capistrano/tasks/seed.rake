# frozen_string_literal: true

namespace :deploy do
  desc 'Runs rake db:seed for SeedMigrations data'
  task :seed do
    on primary :db do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, 'db:seed'
        end
      end
    end
  end
end
