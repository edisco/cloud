# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

u = User.find_or_create_by(email: 'max.nedelchev@gmail.com') do |user|
  user.password = ENV['EDISCO_PASS']
end
u.password = ENV['EDISCO_PASS']
# u.admin = true
u.save!

@test_user = User.create_test_user!

# seed words
wb = WordBuilder.new @test_user.id
date = DateTime.strptime('2017-04-20 12:12:59.123 UTC', Time::DATE_FORMATS[:iso8601ms])
uw1 = wb.add_word text: 'cat', language: Language.eng, created_at: date, updated_at: date
uw2 = wb.add_word text: 'кот', language: Language.rus, created_at: date, updated_at: date

uwA = wb.add_word text: 'dog', language: Language.eng, created_at: date, updated_at: date
uwB = wb.add_word text: 'собака', language: Language.rus, created_at: date, updated_at: date

wb.add_word text: 'hamster', language: Language.eng, created_at: date, updated_at: date
wb.add_word text: 'хомяк', language: Language.rus, created_at: date, updated_at: date

wb.add_word text: 'net', language: Language.eng, created_at: date, updated_at: date
wb.add_word text: 'чистый доход', language: Language.rus, created_at: date, updated_at: date
wb.add_word text: 'сеть', language: Language.rus, created_at: date, updated_at: date

wb.add_word text: 'See', language: Language.ger, gender: 1, created_at: date, updated_at: date + 1
wb.add_word text: 'See', language: Language.ger, gender: 2, created_at: date, updated_at: date + 1


front1 = wb.add_word text: 'fly', language: Language.eng, created_at: date, updated_at: date
front2 = wb.add_word text: 'flew', language: Language.eng, created_at: date, updated_at: date
front3 = wb.add_word text: 'flown', language: Language.eng, created_at: date, updated_at: date
back = wb.add_word text: 'летать', language: Language.rus, created_at: date, updated_at: date

wt = WordTriple.find_or_create_by! front1: front1.word, front2: front2.word, front3: front3.word, back: back.word, created_at: date
UserWordTriple.find_or_create_by!  front1: front1, front2: front2, front3: front3, back: back, word_triple: wt, user: @test_user, created_at: date, updated_at: date

front1 = wb.add_word text: 'dig', language: Language.eng, created_at: date, updated_at: date
front2 = wb.add_word text: 'dug', language: Language.eng, created_at: date, updated_at: date
front3 = wb.add_word text: 'dug', language: Language.eng, created_at: date, updated_at: date
back = wb.add_word text: 'копать', language: Language.rus, created_at: date, updated_at: date
wt = WordTriple.find_or_create_by! front1: front1.word, front2: front2.word, front3: front3.word, back: back.word, created_at: date
uwt = UserWordTriple.find_or_create_by! front1: front1, front2: front2, front3: front3, back: back, word_triple: wt, user: @test_user, created_at: date, updated_at: date

wp = WordPair.find_or_create_by! front_id: uw1.word_id, back_id: uw2.word_id, created_at: date
@test_user.word_pairs.destroy_all
uwp = UserWordPair.find_or_create_by! user: @test_user, word_pair: wp, front: uw1, back: uw2, created_at: date, updated_at: date


UserWordPairHistory.find_or_create_by! user: @test_user, user_word_pair: uwp, correct: true, created_at: DateTime.now - 1, updated_at: date
UserWordPairHistory.find_or_create_by! user: @test_user, user_word_pair: uwp, correct: true, trashed: true, created_at: DateTime.now - 10, updated_at: date

wp = WordPair.find_or_create_by! front_id: uwA.word_id, back_id: uwB.word_id, created_at: date
uwp = UserWordPair.find_or_create_by! user: @test_user, word_pair: wp, front: uwA, back: uwB, created_at: date, updated_at: date

UserWordTripleHistory.find_or_create_by! user: @test_user, user_word_triple: uwt, correct: true, created_at: DateTime.now - 1, updated_at: date
UserWordTripleHistory.find_or_create_by! user: @test_user, user_word_triple: uwt, correct: true, trashed: true, created_at: DateTime.now - 2, updated_at: date
UserWordTripleHistory.find_or_create_by! user: @test_user, user_word_triple: uwt, correct: true, created_at: DateTime.now - 3, updated_at: date
UserWordTripleHistory.find_or_create_by! user: @test_user, user_word_triple: uwt, correct: false, created_at: DateTime.now - 4, updated_at: date
