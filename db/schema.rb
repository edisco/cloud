# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171019085258) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.string   "author_type"
    t.integer  "author_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree
  end

  create_table "languages", force: :cascade do |t|
    t.string   "lang"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["lang"], name: "index_languages_on_lang", using: :btree
  end

  create_table "logs", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "title"
    t.string   "msg"
    t.integer  "level",      default: 0
    t.text     "backtrace"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["level"], name: "index_logs_on_level", using: :btree
    t.index ["user_id"], name: "index_logs_on_user_id", using: :btree
  end

  create_table "tags", force: :cascade do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_tags_on_user_id", using: :btree
  end

  create_table "user_word_pair_histories", force: :cascade do |t|
    t.boolean  "correct"
    t.integer  "user_word_pair_id"
    t.integer  "user_id"
    t.datetime "created_at",                                              null: false
    t.datetime "updated_at",                                              null: false
    t.uuid     "uuid",              default: -> { "uuid_generate_v4()" }
    t.boolean  "trashed",           default: false
    t.integer  "version",           default: 0
    t.index ["trashed"], name: "index_user_word_pair_histories_on_trashed", using: :btree
    t.index ["user_id"], name: "index_user_word_pair_histories_on_user_id", using: :btree
    t.index ["user_word_pair_id"], name: "index_user_word_pair_histories_on_user_word_pair_id", using: :btree
  end

  create_table "user_word_pairs", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "word_pair_id"
    t.uuid     "uuid",         default: -> { "uuid_generate_v4()" }
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.integer  "version",      default: 0
    t.integer  "order",        default: 0
    t.integer  "stage",        default: 0
    t.boolean  "trashed",      default: false
    t.integer  "front_id",                                           null: false
    t.integer  "back_id",                                            null: false
    t.index ["back_id"], name: "index_user_word_pairs_on_back_id", using: :btree
    t.index ["front_id"], name: "index_user_word_pairs_on_front_id", using: :btree
    t.index ["trashed"], name: "index_user_word_pairs_on_trashed", using: :btree
    t.index ["user_id"], name: "index_user_word_pairs_on_user_id", using: :btree
    t.index ["word_pair_id", "user_id"], name: "index_user_word_pairs_on_word_pair_id_and_user_id", unique: true, using: :btree
    t.index ["word_pair_id"], name: "index_user_word_pairs_on_word_pair_id", using: :btree
  end

  create_table "user_word_triple_histories", force: :cascade do |t|
    t.boolean  "correct"
    t.integer  "user_word_triple_id"
    t.integer  "user_id"
    t.datetime "created_at",                                                null: false
    t.datetime "updated_at",                                                null: false
    t.uuid     "uuid",                default: -> { "uuid_generate_v4()" }
    t.boolean  "trashed",             default: false
    t.integer  "version",             default: 0
    t.index ["trashed"], name: "index_user_word_triple_histories_on_trashed", using: :btree
    t.index ["user_id"], name: "index_user_word_triple_histories_on_user_id", using: :btree
    t.index ["user_word_triple_id"], name: "index_user_word_triple_histories_on_user_word_triple_id", using: :btree
  end

  create_table "user_word_triples", force: :cascade do |t|
    t.integer  "word_triple_id"
    t.integer  "version",        default: 0
    t.boolean  "trashed",        default: false
    t.integer  "stage",          default: 0
    t.uuid     "uuid",           default: -> { "uuid_generate_v4()" }
    t.integer  "user_id"
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.integer  "front1_id",                                            null: false
    t.integer  "front2_id",                                            null: false
    t.integer  "front3_id",                                            null: false
    t.integer  "back_id",                                              null: false
    t.index ["back_id"], name: "index_user_word_triples_on_back_id", using: :btree
    t.index ["front1_id"], name: "index_user_word_triples_on_front1_id", using: :btree
    t.index ["front2_id"], name: "index_user_word_triples_on_front2_id", using: :btree
    t.index ["front3_id"], name: "index_user_word_triples_on_front3_id", using: :btree
    t.index ["trashed"], name: "index_user_word_triples_on_trashed", using: :btree
    t.index ["user_id"], name: "index_user_word_triples_on_user_id", using: :btree
    t.index ["word_triple_id", "user_id"], name: "index_user_word_triples_on_word_triple_id_and_user_id", unique: true, using: :btree
    t.index ["word_triple_id"], name: "index_user_word_triples_on_word_triple_id", using: :btree
  end

  create_table "user_words", force: :cascade do |t|
    t.integer  "word_id",                                          null: false
    t.integer  "user_id"
    t.uuid     "uuid",       default: -> { "uuid_generate_v4()" }
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.integer  "version",    default: 0
    t.boolean  "trashed",    default: false
    t.index ["trashed"], name: "index_user_words_on_trashed", using: :btree
    t.index ["user_id"], name: "index_user_words_on_user_id", using: :btree
    t.index ["version"], name: "index_user_words_on_version", using: :btree
    t.index ["word_id", "user_id"], name: "index_user_words_on_word_id_and_user_id", unique: true, using: :btree
    t.index ["word_id"], name: "index_user_words_on_word_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",   null: false
    t.string   "encrypted_password",     default: "",   null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "google_token"
    t.string   "token"
    t.boolean  "new_user",               default: true
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["token"], name: "index_users_on_token", using: :btree
  end

  create_table "word_pair_tags", force: :cascade do |t|
    t.integer  "tag_id"
    t.integer  "word_pair_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["tag_id"], name: "index_word_pair_tags_on_tag_id", using: :btree
    t.index ["word_pair_id"], name: "index_word_pair_tags_on_word_pair_id", using: :btree
  end

  create_table "word_pairs", force: :cascade do |t|
    t.integer  "back_id"
    t.integer  "front_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["back_id"], name: "index_word_pairs_on_back_id", using: :btree
    t.index ["front_id", "back_id"], name: "index_word_pairs_on_front_id_and_back_id", unique: true, using: :btree
    t.index ["front_id"], name: "index_word_pairs_on_front_id", using: :btree
  end

  create_table "word_triples", force: :cascade do |t|
    t.integer  "front1_id"
    t.integer  "front2_id"
    t.integer  "front3_id"
    t.integer  "back_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["back_id"], name: "index_word_triples_on_back_id", using: :btree
    t.index ["front1_id", "front2_id", "front3_id", "back_id"], name: "word_triples_uniqness", unique: true, using: :btree
    t.index ["front1_id"], name: "index_word_triples_on_front1_id", using: :btree
    t.index ["front2_id"], name: "index_word_triples_on_front2_id", using: :btree
    t.index ["front3_id"], name: "index_word_triples_on_front3_id", using: :btree
  end

  create_table "words", force: :cascade do |t|
    t.string   "text",                                              null: false
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.integer  "language_id",                                       null: false
    t.integer  "part",        default: 0
    t.integer  "gender",      default: 0
    t.uuid     "uuid",        default: -> { "uuid_generate_v4()" }
    t.index ["gender"], name: "index_words_on_gender", using: :btree
    t.index ["language_id"], name: "index_words_on_language_id", using: :btree
    t.index ["part"], name: "index_words_on_part", using: :btree
    t.index ["text", "language_id", "part", "gender"], name: "index_words_on_text_and_language_id_and_part_and_gender", unique: true, using: :btree
  end

  add_foreign_key "logs", "users"
  add_foreign_key "tags", "users"
  add_foreign_key "user_word_pair_histories", "user_word_pairs"
  add_foreign_key "user_word_pair_histories", "users"
  add_foreign_key "user_word_pairs", "user_words", column: "back_id"
  add_foreign_key "user_word_pairs", "user_words", column: "front_id"
  add_foreign_key "user_word_pairs", "users"
  add_foreign_key "user_word_pairs", "word_pairs"
  add_foreign_key "user_word_triple_histories", "user_word_triples"
  add_foreign_key "user_word_triple_histories", "users"
  add_foreign_key "user_word_triples", "user_words", column: "back_id"
  add_foreign_key "user_word_triples", "user_words", column: "front1_id"
  add_foreign_key "user_word_triples", "user_words", column: "front2_id"
  add_foreign_key "user_word_triples", "user_words", column: "front3_id"
  add_foreign_key "user_word_triples", "users"
  add_foreign_key "user_word_triples", "word_triples"
  add_foreign_key "user_words", "users"
  add_foreign_key "user_words", "words"
  add_foreign_key "word_pair_tags", "tags"
  add_foreign_key "word_pair_tags", "word_pairs"
  add_foreign_key "word_pairs", "words", column: "back_id"
  add_foreign_key "word_pairs", "words", column: "front_id"
  add_foreign_key "word_triples", "words", column: "back_id"
  add_foreign_key "word_triples", "words", column: "front1_id"
  add_foreign_key "word_triples", "words", column: "front2_id"
  add_foreign_key "word_triples", "words", column: "front3_id"
  add_foreign_key "words", "languages"
end
