class CreateLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :logs do |t|
      t.references :user, foreign_key: true
      t.string :title
      t.string :msg
      t.integer :level, default: 0
      t.text :backtrace

      t.timestamps
    end
    add_index :logs, :level
  end
end
