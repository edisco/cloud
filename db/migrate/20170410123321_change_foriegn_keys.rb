class ChangeForiegnKeys < ActiveRecord::Migration[5.0]
  def change
    remove_foreign_key :user_word_pair_histories, :word_pairs
    remove_foreign_key :user_word_triple_histories, :word_triples
    add_foreign_key :user_word_pair_histories, :user_word_pairs
    add_foreign_key :user_word_triple_histories, :user_word_triples
  end
end
