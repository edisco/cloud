class CreateWordPairs < ActiveRecord::Migration[5.0]
  def change
    create_table :word_pairs do |t|
      t.integer :back_id
      t.integer :front_id
      t.references :user, foreign_key: true

      t.timestamps
      t.foreign_key :words, column: :front_id
      t.foreign_key :words, column: :back_id
    end
    add_index :word_pairs, :back_id
    add_index :word_pairs, :front_id
  end
end
