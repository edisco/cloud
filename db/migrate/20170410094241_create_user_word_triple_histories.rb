class CreateUserWordTripleHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :user_word_triple_histories do |t|
      t.boolean :correct
      t.references :word_triple, foreign_key: true
      t.references :user, foreign_key: true
      t.timestamps
    end
  end
end
