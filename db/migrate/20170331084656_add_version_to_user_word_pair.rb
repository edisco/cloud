class AddVersionToUserWordPair < ActiveRecord::Migration[5.0]
  def change
    add_column :user_word_pairs, :version, :integer, default: 0
    add_column :user_word_pairs, :order, :integer, default: 0
    add_column :user_word_pairs, :stage, :integer, default: 0
    add_column :user_word_pairs, :trashed, :boolean, default: false
  end
end
