class AddVersionFieldToHistories < ActiveRecord::Migration[5.0]
  def change
    add_column :user_word_pair_histories, :version, :integer, default: 0
    add_column :user_word_triple_histories, :version, :integer, default: 0
  end
end
