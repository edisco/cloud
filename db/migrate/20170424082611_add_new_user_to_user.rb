class AddNewUserToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :new_user, :boolean, default: true
  end
end
