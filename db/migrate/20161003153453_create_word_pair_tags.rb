class CreateWordPairTags < ActiveRecord::Migration[5.0]
  def change
    create_table :word_pair_tags do |t|
      t.references :tag, foreign_key: true
      t.references :word_pair, foreign_key: true

      t.timestamps
    end
  end
end
