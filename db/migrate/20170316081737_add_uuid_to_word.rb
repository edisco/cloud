class AddUuidToWord < ActiveRecord::Migration[5.0]
  def change
    add_column :words, :uuid, :uuid, default: 'uuid_generate_v4()'
  end
end
