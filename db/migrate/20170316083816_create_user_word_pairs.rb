class CreateUserWordPairs < ActiveRecord::Migration[5.0]
  def change
    create_table :user_word_pairs do |t|
      t.references :user, foreign_key: true
      t.references :word_pair, foreign_key: true
      t.uuid :uuid, default: 'uuid_generate_v4()'

      t.timestamps
    end
    remove_column :word_pairs, :user_id
  end
end
