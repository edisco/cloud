class AddVersionToWord < ActiveRecord::Migration[5.0]
  def change
    add_column :user_words, :version, :integer, default: 0
    add_index :user_words, :version
  end
end
