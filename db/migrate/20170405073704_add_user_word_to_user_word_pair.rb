class AddUserWordToUserWordPair < ActiveRecord::Migration[5.0]
  def change
    add_column :user_word_pairs, :front_id, :integer
    add_index :user_word_pairs, :front_id
    add_column :user_word_pairs, :back_id, :integer
    add_index :user_word_pairs, :back_id
    add_foreign_key :user_word_pairs,  :user_words, column: :front_id
    add_foreign_key :user_word_pairs,  :user_words, column: :back_id
  end
end
