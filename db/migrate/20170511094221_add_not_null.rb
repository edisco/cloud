class AddNotNull < ActiveRecord::Migration[5.0]
  def change
  	change_column_null :user_words, :word_id, false
  	change_column_null :user_word_pairs, :front_id, false
  	change_column_null :user_word_pairs, :back_id, false

   	change_column_null :user_word_triples, :front1_id, false
  	change_column_null :user_word_triples, :front2_id, false
  	change_column_null :user_word_triples, :front3_id, false
  	change_column_null :user_word_triples, :back_id, false
  end
end

