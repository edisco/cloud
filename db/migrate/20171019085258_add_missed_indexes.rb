class AddMissedIndexes < ActiveRecord::Migration[5.0]
  def change
    add_index :user_word_pairs, :trashed
    add_index :user_word_triples, :trashed
  end
end
