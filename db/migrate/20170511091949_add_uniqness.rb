class AddUniqness < ActiveRecord::Migration[5.0]
  def change
  		add_index :words, [:text, :language_id, :part, :gender], :unique => true
  		add_index :word_pairs, [:front_id, :back_id], :unique => true
  		add_index :word_triples, [:front1_id, :front2_id, :front3_id, :back_id], :unique => true, name: "word_triples_uniqness"
  		add_index :user_words, [:word_id, :user_id], :unique => true
  		add_index :user_word_pairs, [:word_pair_id, :user_id], :unique => true
  		add_index :user_word_triples, [:word_triple_id, :user_id], :unique => true
  end
end
