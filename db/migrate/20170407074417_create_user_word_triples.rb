class CreateUserWordTriples < ActiveRecord::Migration[5.0]
  def change
    create_table :user_word_triples do |t|
      t.references :word_triple, foreign_key: true
      t.integer :version, default: 0
      t.boolean :trashed, default: false
      t.integer :stage, default: 0
      t.uuid 'uuid', default: -> { 'uuid_generate_v4()' }
      t.integer :user_id

      t.timestamps
    end
    add_foreign_key 'user_word_triples', 'users'
    add_index :user_word_triples, :user_id
  end
end
