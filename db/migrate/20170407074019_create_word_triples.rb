class CreateWordTriples < ActiveRecord::Migration[5.0]
  def change
    create_table :word_triples do |t|
      t.integer :front1_id
      t.integer :front2_id
      t.integer :front3_id
      t.integer :back_id
      t.timestamps
      t.foreign_key :words, column: :front1_id
      t.foreign_key :words, column: :front2_id
      t.foreign_key :words, column: :front3_id
      t.foreign_key :words, column: :back_id
    end
    add_index :word_triples, :front1_id
    add_index :word_triples, :front2_id
    add_index :word_triples, :front3_id
    add_index :word_triples, :back_id
  end
end
