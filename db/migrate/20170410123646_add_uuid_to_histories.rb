class AddUuidToHistories < ActiveRecord::Migration[5.0]
  def change
    add_column :user_word_pair_histories, :uuid, :uuid, default: 'uuid_generate_v4()'
    add_column :user_word_triple_histories, :uuid, :uuid, default: 'uuid_generate_v4()'
  end
end
