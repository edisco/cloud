class AddRestrictionToWord < ActiveRecord::Migration[5.0]
  def change
    change_column_null :words, :text, false
    change_column_null :words, :language_id, false
    change_column_default :words, :part, 0
    change_column_default :words, :gender, 0
  end
end
