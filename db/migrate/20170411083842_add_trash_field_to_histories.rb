class AddTrashFieldToHistories < ActiveRecord::Migration[5.0]
  def change
    add_column :user_word_pair_histories, :trashed, :boolean, default: false
    add_index :user_word_pair_histories, :trashed
    add_column :user_word_triple_histories, :trashed, :boolean, default: false
    add_index :user_word_triple_histories, :trashed
  end
end
