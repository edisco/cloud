class AddTrashedToUserWord < ActiveRecord::Migration[5.0]
  def change
    add_column :user_words, :trashed, :boolean, default: false
    add_index :user_words, :trashed
  end
end
