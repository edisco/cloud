class AddFieldsToUserWordTriple < ActiveRecord::Migration[5.0]
  def change
    add_column :user_word_triples, :front1_id, :integer
    add_index :user_word_triples, :front1_id
    add_column :user_word_triples, :front2_id, :integer
    add_index :user_word_triples, :front2_id
    add_column :user_word_triples, :front3_id, :integer
    add_index :user_word_triples, :front3_id
    add_column :user_word_triples, :back_id, :integer
    add_index :user_word_triples, :back_id

    add_foreign_key :user_word_triples,  :user_words, column: :front1_id
    add_foreign_key :user_word_triples,  :user_words, column: :front2_id
    add_foreign_key :user_word_triples,  :user_words, column: :front3_id
    add_foreign_key :user_word_triples,  :user_words, column: :back_id
  end
end
