class AddLangToWord < ActiveRecord::Migration[5.0]
  def change
    add_reference :words, :language, foreign_key: true
    add_column :words, :part, :integer
    add_index :words, :part
    add_column :words, :gender, :integer
    add_index :words, :gender
  end
end
