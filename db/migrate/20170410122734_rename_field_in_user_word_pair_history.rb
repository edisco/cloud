class RenameFieldInUserWordPairHistory < ActiveRecord::Migration[5.0]
  def change
    rename_column :user_word_pair_histories, :word_pair_id, :user_word_pair_id
    rename_column :user_word_triple_histories, :word_triple_id, :user_word_triple_id
  end
end
