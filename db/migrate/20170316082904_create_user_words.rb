class CreateUserWords < ActiveRecord::Migration[5.0]
  def change
    create_table :user_words do |t|
      t.references :word, foreign_key: true
      t.references :user, foreign_key: true
      t.uuid :uuid, default: 'uuid_generate_v4()'

      t.timestamps
    end
  end
end
